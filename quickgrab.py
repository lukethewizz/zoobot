from PIL import ImageGrab
from PIL import ImageOps
import os
import time
import pyautogui

#Takes a screenshow of the browser game window for further use
def screenGrab():
    box = (0,0,1694,1029)
    im = ImageGrab.grab(box)
    im.save(os.getcwd() + '\\full_snap__' + str(int(time.time())) +
'.png', 'PNG')


#Takes a screenshot of specified window and saves it
def grab(box):
    im = ImageOps.grayscale(ImageGrab.grab(box))
    im.save(os.getcwd() + '\\full_snap__' + str(int(time.time())) +
'.png', 'PNG')


#Returns RGB pixel value (Img turned gray for time efficiency)
def load_img(box):
    im = ImageOps.grayscale(ImageGrab.grab(box))
    a = list(im.getcolors())
    val = 0
    for item in a:
        i = item[0]*item[1]
        val +=i
    return val
