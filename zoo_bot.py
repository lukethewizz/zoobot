import requests
import os
import time
import pyautogui
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from mouse_commands import leftClick,mousePos
from coords import boxes, positions, pixel_values
from quickgrab import screenGrab, load_img
from friends import friends_help

def main():

    #Opening chrome session with maximized window,with google account logged in
    options = webdriver.ChromeOptions()
    options.add_argument(os.environ['path_to_chrome_session'])
    options.add_argument('--start-maximized')

    driver = webdriver.Chrome(
                              executable_path=os.environ['driver_path'],
                              options=options
                              )
    url = 'https://zoo2app.upjers.com/game/'
    driver.get(url)

    #getting rid of 'Chrome is being controled by automated software' message.
    #Did not figure out the better way
    mousePos((1892,162))
    time.sleep(.5)
    leftClick()
    time.sleep(2)

    #Getting login and password values from enviroment variables
    login = os.environ['zoo_login']
    password = os.environ['zoo_pass']

    #filling out login form
    driver.find_element_by_class_name('login-username').send_keys(login)
    driver.find_element_by_class_name('login-password').send_keys(password)
    driver.find_element_by_class_name('login-submit').click()

    #entering the game
    game_tickbox(pixel_values['start_checkmark'],positions['start_checkmark'])
    time.sleep(7)

    #closing adverts
    for i in range(5):
        close_advert()
        time.sleep(2)
    time.sleep(10)
    for i in range(5):
        close_advert()
        time.sleep(2)

    friends_help()
    driver.close()

#waiting until game is fully loaded and enters it
def game_tickbox(px_value,mouse_position):
    while True:
        if load_img(boxes['start_checkmark']) == px_value:
            mousePos(mouse_position)
            time.sleep(.2)
            leftClick()
            break
        time.sleep(1)

#Identifies if adverts are present and closes them
def close_advert():
    btn = pyautogui.locateOnScreen('images/xmark.png',
                                    grayscale=True,
                                    confidence=0.9,
                                    region=(800,167,650,250)
                                    )
    try:
        x,y = pyautogui.center(btn)
        pyautogui.click(x,y)
    except:
        pass


if __name__ == '__main__':
    main()
