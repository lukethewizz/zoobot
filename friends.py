import time
from quickgrab import grab,screenGrab, load_img
import pyautogui
from coords import *
from mouse_commands import leftClick,mousePos,leftDown,leftUp


def friends_help():
    #targeting arrow_down
    mousePos((1629,197))
    time.sleep(1)
    leftClick(2)

    #targeting friends icon
    mousePos((1624,712))
    time.sleep(1.5)
    leftClick(2)

    #targeting help button
    mousePos((784,771))
    time.sleep(1)
    leftClick()

    for i in range(17):
        time.sleep(7)
        #recognizing which minievent is present in friend's zoo
        if load_img(boxes['friends']) == pixel_values['friends_item_crow']:
            crows()

        elif load_img(boxes['friends']) == pixel_values['friends_item_baloon']:
            baloons()

        elif load_img(boxes['friends']) == pixel_values['friends_item_net']:
            animals()

        time.sleep(1)

        #When all zoos are taken care of, return to yours, keep going until then
        if load_img(boxes['go_home']) == pixel_values['go_home']:
            mousePos(positions['go_home'])
            time.sleep(.1)
            leftClick()
            break

        else:
            mousePos(positions['next_zoo_btn'])
            time.sleep(.1)
            leftClick()

def crows():
    while load_img(boxes['zoo_indicator']) == pixel_values['zoo_indicator']:
        mousePos(positions['friends_item'])
        time.sleep(.5)
        leftClick()
        time.sleep(1)
        mousePos(positions['friends_target'])
        time.sleep(.1)
        leftClick(6)
        time.sleep(.5)

def baloons():
    while load_img(boxes['zoo_indicator']) == pixel_values['zoo_indicator']:
        mousePos(positions['friends_item'])
        time.sleep(1)
        leftClick(2)
        mousePos(positions['friends_baloon'])
        time.sleep(.3)
        leftDown()
        time.sleep(1.1)
        mousePos(positions['friends_target'])
        time.sleep(.5)
        leftUp()
        time.sleep(.6)


def animals():
    while load_img(boxes['zoo_indicator']) == pixel_values['zoo_indicator']:
        mousePos(positions['friends_item'])
        time.sleep(.5)
        leftClick()
        time.sleep(.1)
        mousePos(positions['friends_net'])
        leftDown()
        time.sleep(.65)
        mousePos((1218,731))
        time.sleep(.1)

        pyautogui.moveRel(-550, -200, duration = 0.25)
        pyautogui.moveRel(0, -40, duration = 0.1)
        pyautogui.moveRel(550, 200, duration = 0.25)
        mousePos((1140,321))
        time.sleep(.1)
        pyautogui.moveRel(-500, 600, duration = 0.25)
        pyautogui.moveRel(-60, -20, duration = 0.2)
        pyautogui.moveRel(500, -600, duration = 0.25)
        leftUp()
        time.sleep(.5)
