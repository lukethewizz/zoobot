boxes = {
            'start_checkmark' : (920,755,940,777),
            'special_offer_X' : (1250,227,1315,274),
            'novinky_X' : (1342,193,1410,235),
            'diamond_action1' : (1334,206,1399,254),
            'friends' : (560, 917, 628, 982),
            'chest_amount' : (750,677,810,717),
            'task_done' : (800,560,820,580),
            'zoo_indicator' : (1068,926,1135,974),
            'go_home' : (920,820,970,870),
            'breed' : (700,940,740,970)
}


positions = {
                'diamond_action1' : (1093,180),
                'start_checkmark' :(852,645),
                'special_offer_X' :(1028,205),
                'novinky_X': (1094,169),
                'friends_item' : (599,943),
                'friends_target':(868,571),
                'friends_baloon':(1569,817),
                'friends_net' : (1566,882),
                'chest': (813,540),
                'bigger_chest' : (674,499),
                'prizes_pick_up' : (856,670),
                'next_zoo_btn' : (750,850),
                'go_home' : (874,853),
                'breed' : (700,950)
}


pixel_values = {
                    'diamond_action1' : 463893,
                    'special_offer_X' : 457481,
                    'novinky_X' : 430140,
                    'start_checkmark' : 74027,
                    'friends_item_baloon' : 659506,
                    'friends_item_crow' : 473872,
                    'friends_item_net' : 539649,
                    'chest_amount' : 309121,
                    'task_done' : 90400,
                    'zoo_indicator' : 499644,
                    'go_home' : 296582,
                    'breed' : 149878
}
